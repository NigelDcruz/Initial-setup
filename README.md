# [Initial- Nigel Dcruz]

## Usage

Edit package .jason file First. Then,

1) run `npm install` 
2) run `gulp`
3) run `gulp dev` which will open up a preview of the template in your default browser, watch for changes to core template files, and live reload the browser when changes are saved. You can view the `gulpfile.js` to see which tasks are included with the dev environment.

#### Gulp Tasks

 make sure to add --save-dev to add gulp as dev dependencies

- `gulp` the default task that builds everything
- `gulp dev` browserSync opens the project in your default browser and live reloads when changes are made
- `gulp sass` compiles SCSS files into CSS
- `gulp minify-css` minifies the compiled CSS file
- `gulp minify-js` minifies the themes JS file
- `gulp copy` copies dependencies from node_modules to the vendor directory
- `gulp typescript` complies typescript into js folder